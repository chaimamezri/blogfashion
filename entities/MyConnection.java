/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blogfashion.testpublication.entities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chaima
 */
public class MyConnection {
    public String url="jdbc:mysql://localhost:3306/blogfashion";
    public String login="root";
    public String pwd="";
    public static Connection cnx ; 
    public static MyConnection myc;
    
      private MyConnection () {
          try {
      cnx=DriverManager.getConnection(url,login,pwd);
      System.out.println("connection etablie");
}
        catch (SQLException ex ){
       System.out.println(ex.getMessage());
       System.out.println("connection non etablie");

        
        }         
}
      
      public static Connection getConnection() {
          return cnx;
      }
      
     
public static MyConnection getInstance(){
    if(myc ==null){
        myc = new MyConnection();
    
    }
    return myc;
}

    static class getInstance extends MyConnection {

        public getInstance() {
        }
    }
    

}
