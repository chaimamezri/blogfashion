/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.blogfashion;

import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.io.JSONParser;
import com.codename1.io.rest.Rest;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

/**
 * GUI builder created Form
 *
 * @author Mezri
 */
public class NewsFeedForm extends com.codename1.ui.Form {

    public NewsFeedForm(){
        this(com.codename1.ui.util.Resources.getGlobalResources());
    }
    
    public NewsFeedForm(Resources resourceObjectInstance) {
        //initGuiBuilderComponents(resourceObjectInstance);
        setLayout(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
        setInlineStylesTheme(resourceObjectInstance);
        setScrollableY(true);
                setInlineStylesTheme(resourceObjectInstance);
        setTitle("News feed");
        setName("TrendingForm");
        gui_Button_2.setText("");
        gui_Button_2.setUIID("Label");
                gui_Button_2.setInlineStylesTheme(resourceObjectInstance);
        gui_Button_2.setName("Button_2");
        gui_Button_2.setText("Add");
        gui_Button_2.setTextPosition(Component.RIGHT);
        FontImage.setMaterialIcon(gui_Button_2, "\ue145".charAt(0));

        addComponent(gui_Button_2);
        
        gui_Button_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                new CreateNewsFeedForm().show();
            }
        });
        
        Map<String, Object> jsonData = Rest.
            get("http://localhost/PI/web/app_dev.php/rest/news").
            acceptJson().
            getAsJsonMap().
            getResponseData();
        
        try {
            System.out.println(jsonData);
            parseResponse(jsonData, resourceObjectInstance);
        } catch(IOException e) {
            
        }
    }
    
    private void parseResponse(Map<String, Object> jsonData, Resources resourceObjectInstance) throws IOException {
        //List<Map<String, Object>> pubsData = (List<Map<String, Object>>)jsonData.get("publications");
        
        String pubsDataJSON = (String)jsonData.get("publications");
        
        JSONParser jsonParser = new JSONParser();
       
        Map<String, Object> root = (Map<String, Object>)
                jsonParser.parseJSON(new InputStreamReader(new ByteArrayInputStream(pubsDataJSON.getBytes()), "UTF-8"));
        
        List<Map<String, Object>> pubsData = (List<Map<String, Object>>)root.get("root");
        
        System.out.println(pubsData);
        
        for(Map<String, Object> pubData : pubsData) {
            
            double id = (double)pubData.get("pubid");
            String title = (String)pubData.get("title");
            String content = (String)pubData.get("content");
            String imageName = (String)pubData.get("image");
            double views = (double)pubData.get("views");
            
            List<Map<String, Object>> likes = (List<Map<String, Object>>)pubData.get("likes");
                        
            EncodedImage placeholder = EncodedImage.createFromImage(Image.createImage(1400, 500, 0xffff0000), true);
            
            URLImage image = URLImage.createToStorage(placeholder, imageName, 
                "http://localhost/PI/web/" + imageName);
        
            addNewsFeedContainer(resourceObjectInstance, (int)id, title, content, image, (int)views, likes);
        }
    }
    
    private void addNewsFeedContainer(Resources resourceObjectInstance, int id, String title, String content,
            URLImage image, int views, List<Map<String, Object>> likes) {
        Container gui_NewNewsFeedContainer = new Container(new BoxLayout(BoxLayout.Y_AXIS));       
        gui_NewNewsFeedContainer.setInlineStylesTheme(resourceObjectInstance);
        gui_NewNewsFeedContainer.setName("NewsFeedContainer");
        addComponent(gui_NewNewsFeedContainer);
        
        Container gui_Container = new Container(new BorderLayout());
        gui_Container.setInlineStylesTheme(resourceObjectInstance);
        gui_Container.setName("Container_1");
        
        Container gui_imageContainer = new Container(new BoxLayout(BoxLayout.X_AXIS));
        gui_imageContainer.setInlineStylesTheme(resourceObjectInstance);
        gui_imageContainer.setName("imageContainer1");
        
        Label gui_separator = new Label();
        gui_separator.setUIID("Separator");
        gui_separator.setInlineStylesTheme(resourceObjectInstance);
        gui_separator.setName("separator1");
        
        gui_NewNewsFeedContainer.addComponent(gui_Container);
        
        MultiButton gui_Multi_Button = new MultiButton();
        gui_Multi_Button.setUIID("Label");
        gui_Multi_Button.setInlineStylesTheme(resourceObjectInstance);
        gui_Multi_Button.setName("Multi_Button_1");
        gui_Multi_Button.setIcon(resourceObjectInstance.getImage("contact-c.png"));
        gui_Multi_Button.setPropertyValue("line1", "Rakia Zribi");
        //gui_Multi_Button.setPropertyValue("line2", "@dropperidiot");
        gui_Multi_Button.setPropertyValue("uiid1", "Label");
        gui_Multi_Button.setPropertyValue("uiid2", "RedLabel");
        
        MultiButton gui_NewLA = new MultiButton();
        gui_NewLA.setUIID("Label");
        gui_NewLA.setInlineStylesTheme(resourceObjectInstance);
        gui_NewLA.setName("LA");
        gui_NewLA.setPropertyValue("line1", "05/01/2020");
       // gui_NewLA.setPropertyValue("line2", "in Los Angeles");
        gui_NewLA.setPropertyValue("uiid1", "SlightlySmallerFontLabel");
        gui_NewLA.setPropertyValue("uiid2", "RedLabelRight");
        
        gui_Container.addComponent(BorderLayout.CENTER, gui_Multi_Button);
        gui_Container.addComponent(BorderLayout.EAST, gui_NewLA);       
        gui_NewNewsFeedContainer.addComponent(gui_imageContainer);
        
        Container gui_Container2 = new Container(new BorderLayout());
        gui_Container2.setInlineStylesTheme(resourceObjectInstance);
        gui_Container2.setName("Container_2");
        
        gui_imageContainer.addComponent(gui_Container2);
        
        TextArea gui_Text_Area = new TextArea();
        gui_Text_Area.setText(content);
        gui_Text_Area.setUIID("SlightlySmallerFontLabelLeft");
        gui_Text_Area.setInlineStylesTheme(resourceObjectInstance);
        gui_Text_Area.setName("Text_Area_1");
        
        ImageViewer gui_ImageViewer = new ImageViewer();
        gui_ImageViewer.setInlineStylesTheme(resourceObjectInstance);
        gui_ImageViewer.setName("Image_Viewer");
        gui_ImageViewer.setImage(image);
        
        /*Button gui_Button = new Button();
        gui_Button.setUIID("Label");
        gui_Button.setInlineStylesTheme(resourceObjectInstance);
        gui_Button.setName("Button_1");
        FontImage.setMaterialIcon(gui_Button,"\ue5c8".charAt(0));*/
        
        final Boolean[] isLiked = new Boolean[1];
        isLiked[0] = !likes.isEmpty();
        Button gui_ButtonLike = new Button();
        gui_ButtonLike.setUIID("Label");
        gui_ButtonLike.setInlineStylesTheme(resourceObjectInstance);
        gui_ButtonLike.setName("Button");
        FontImage.setMaterialIcon(gui_ButtonLike, isLiked[0] ? "\ue87d".charAt(0) : "\ue87e".charAt(0));
        
        gui_ButtonLike.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                Rest.get("http://localhost/PI/web/app_dev.php/news/like?id=" + id).
                        getAsString().getResponseCode();
                
                isLiked[0] = !isLiked[0];
                FontImage.setMaterialIcon(gui_ButtonLike, isLiked[0] ? "\ue87d".charAt(0) : "\ue87e".charAt(0));
            }
        });

        Button gui_ButtonViews = new Button();
        gui_ButtonViews.setUIID("Label");
        gui_ButtonViews.setInlineStylesTheme(resourceObjectInstance);
        gui_ButtonViews.setName("Button");
        FontImage.setMaterialIcon(gui_ButtonViews,"\ue417".charAt(0));
                
        Label gui_NewLabelViews = new Label();
        gui_NewLabelViews.setText("" + views);
        gui_NewLabelViews.setInlineStylesTheme(resourceObjectInstance);
        gui_NewLabelViews.setName("LabelViews");
        
        Button gui_ButtonDelete = new Button();
        gui_ButtonDelete.setUIID("Label");
        gui_ButtonDelete.setInlineStylesTheme(resourceObjectInstance);
        gui_ButtonDelete.setName("Button");
        FontImage.setMaterialIcon(gui_ButtonDelete, "\ue872".charAt(0));
        
        gui_ButtonDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                
                Rest.get("http://localhost/PI/web/app_dev.php/rest/news/delete?id=" + id).
                    getAsString().getResponseCode();

                new NewsFeedForm(resourceObjectInstance).show();
  
            }
        });
        
        Button gui_ButtonEdit = new Button();
        gui_ButtonEdit.setUIID("Label");
        gui_ButtonEdit.setInlineStylesTheme(resourceObjectInstance);
        gui_ButtonEdit.setName("Button");
        FontImage.setMaterialIcon(gui_ButtonEdit, "\ue254".charAt(0));
        
        gui_ButtonEdit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                Rest.get("http://localhost/PI/web/app_dev.php/news/delete?id=" + id).
                        getAsString().getResponseCode();
                
                new CreateNewsFeedForm(id, title, content).show();
            }
        });
        
        Container gui_Box_LayoutX = new Container(new BoxLayout(BoxLayout.X_AXIS));  
        gui_Box_LayoutX.addComponent(gui_ButtonLike);
        gui_Box_LayoutX.addComponent(gui_ButtonViews);
        gui_Box_LayoutX.addComponent(gui_NewLabelViews);
        gui_Box_LayoutX.addComponent(gui_ButtonDelete);
        gui_Box_LayoutX.addComponent(gui_ButtonEdit);
        
        gui_Container2.addComponent(BorderLayout.NORTH, gui_ImageViewer);
        gui_Container2.addComponent(BorderLayout.CENTER, gui_Text_Area);
        gui_Container2.addComponent(BorderLayout.SOUTH, gui_Box_LayoutX);
        //gui_Container2.addComponent(BorderLayout.EAST, gui_Button);
        gui_NewNewsFeedContainer.addComponent(gui_separator);
        
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////-- DON'T EDIT BELOW THIS LINE!!!
    protected com.codename1.ui.Button gui_Button_2 = new com.codename1.ui.Button();
    protected com.codename1.ui.Container gui_NewsFeedContainer = new com.codename1.ui.Container(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
    protected com.codename1.ui.Container gui_Container_1 = new com.codename1.ui.Container(new com.codename1.ui.layouts.BorderLayout());
    protected com.codename1.components.MultiButton gui_Multi_Button_1 = new com.codename1.components.MultiButton();
    protected com.codename1.components.MultiButton gui_LA = new com.codename1.components.MultiButton();
    protected com.codename1.ui.Container gui_imageContainer1 = new com.codename1.ui.Container(new com.codename1.ui.layouts.BorderLayout());
    protected com.codename1.ui.Container gui_Container_2 = new com.codename1.ui.Container(new com.codename1.ui.layouts.BorderLayout());
    protected com.codename1.components.ImageViewer gui_Image_Viewer = new com.codename1.components.ImageViewer();
    protected com.codename1.ui.TextArea gui_Text_Area_1 = new com.codename1.ui.TextArea();
    protected com.codename1.ui.Container gui_Box_Layout_X = new com.codename1.ui.Container(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.X_AXIS));
    protected com.codename1.ui.Button gui_Button = new com.codename1.ui.Button();
    protected com.codename1.ui.Button gui_Button_1 = new com.codename1.ui.Button();
    protected com.codename1.ui.Label gui_LabelViews = new com.codename1.ui.Label();
    protected com.codename1.ui.Label gui_separator1 = new com.codename1.ui.Label();


// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        setLayout(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
        setInlineStylesTheme(resourceObjectInstance);
        setScrollableY(true);
                setInlineStylesTheme(resourceObjectInstance);
        setTitle("Trending");
        setName("TrendingForm");
        gui_Button_2.setUIID("Label");
                gui_Button_2.setInlineStylesTheme(resourceObjectInstance);
        gui_Button_2.setName("Button_2");
                gui_NewsFeedContainer.setInlineStylesTheme(resourceObjectInstance);
        gui_NewsFeedContainer.setName("NewsFeedContainer");
        addComponent(gui_Button_2);
        addComponent(gui_NewsFeedContainer);
                gui_Container_1.setInlineStylesTheme(resourceObjectInstance);
        gui_Container_1.setName("Container_1");
                gui_imageContainer1.setInlineStylesTheme(resourceObjectInstance);
        gui_imageContainer1.setName("imageContainer1");
        gui_separator1.setUIID("Separator");
                gui_separator1.setInlineStylesTheme(resourceObjectInstance);
        gui_separator1.setName("separator1");
        gui_NewsFeedContainer.addComponent(gui_Container_1);
        gui_Multi_Button_1.setUIID("Label");
                gui_Multi_Button_1.setInlineStylesTheme(resourceObjectInstance);
        gui_Multi_Button_1.setName("Multi_Button_1");
        gui_Multi_Button_1.setIcon(resourceObjectInstance.getImage("contact-c.png"));
        gui_Multi_Button_1.setPropertyValue("line1", "Ami Koehler");
        gui_Multi_Button_1.setPropertyValue("line2", "@dropperidiot");
        gui_Multi_Button_1.setPropertyValue("uiid1", "Label");
        gui_Multi_Button_1.setPropertyValue("uiid2", "RedLabel");
        gui_LA.setUIID("Label");
                gui_LA.setInlineStylesTheme(resourceObjectInstance);
        gui_LA.setName("LA");
        gui_LA.setPropertyValue("line1", "3 minutes ago");
        gui_LA.setPropertyValue("line2", "in Los Angeles");
        gui_LA.setPropertyValue("uiid1", "SlightlySmallerFontLabel");
        gui_LA.setPropertyValue("uiid2", "RedLabelRight");
        gui_Container_1.addComponent(com.codename1.ui.layouts.BorderLayout.CENTER, gui_Multi_Button_1);
        gui_Container_1.addComponent(com.codename1.ui.layouts.BorderLayout.EAST, gui_LA);
        gui_NewsFeedContainer.addComponent(gui_imageContainer1);
                gui_Container_2.setInlineStylesTheme(resourceObjectInstance);
        gui_Container_2.setName("Container_2");
        gui_imageContainer1.addComponent(com.codename1.ui.layouts.BorderLayout.SOUTH, gui_Container_2);
                gui_Image_Viewer.setInlineStylesTheme(resourceObjectInstance);
        gui_Image_Viewer.setName("Image_Viewer");
        gui_Text_Area_1.setText("The park is a favorite among skaters in California and it definitely deserves it. The park is complete with plenty of smooth banks to gain a ton of speed in the flow bowl.");
        gui_Text_Area_1.setUIID("Label");
                gui_Text_Area_1.setInlineStylesTheme(resourceObjectInstance);
        gui_Text_Area_1.setName("Text_Area_1");
                gui_Box_Layout_X.setInlineStylesTheme(resourceObjectInstance);
        gui_Box_Layout_X.setName("Box_Layout_X");
        gui_Container_2.addComponent(com.codename1.ui.layouts.BorderLayout.NORTH, gui_Image_Viewer);
        gui_Container_2.addComponent(com.codename1.ui.layouts.BorderLayout.EAST, gui_Text_Area_1);
        gui_Container_2.addComponent(com.codename1.ui.layouts.BorderLayout.SOUTH, gui_Box_Layout_X);
        gui_Button.setText("Button");
        gui_Button.setUIID("Label");
                gui_Button.setInlineStylesTheme(resourceObjectInstance);
        gui_Button.setName("Button");
        gui_Button_1.setText("Button");
        gui_Button_1.setUIID("Label");
                gui_Button_1.setInlineStylesTheme(resourceObjectInstance);
        gui_Button_1.setName("Button_1");
        gui_LabelViews.setText("10");
                gui_LabelViews.setInlineStylesTheme(resourceObjectInstance);
        gui_LabelViews.setName("LabelViews");
        gui_Box_Layout_X.addComponent(gui_Button);
        gui_Box_Layout_X.addComponent(gui_Button_1);
        gui_Box_Layout_X.addComponent(gui_LabelViews);
        gui_NewsFeedContainer.addComponent(gui_separator1);
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!
}
