/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.blogfashion;

import com.codename1.components.InfiniteProgress;
import com.codename1.io.File;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.Log;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.io.Storage;
import com.codename1.io.Util;
import com.codename1.io.rest.Rest;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * GUI builder created Form
 *
 * @author Anis
 */
public class CreateNewsFeedForm extends com.codename1.ui.Form {
    
    public CreateNewsFeedForm() {
        this(com.codename1.ui.util.Resources.getGlobalResources(), false, 0, "", "");
    }
    
    public CreateNewsFeedForm(int id, String title, String content) {
        this(com.codename1.ui.util.Resources.getGlobalResources(), true, id, title, content);
        
        
    }
    
    public CreateNewsFeedForm(com.codename1.ui.util.Resources resourceObjectInstance, boolean isEdit, int id, String title, 
            String content) {
        initGuiBuilderComponents(resourceObjectInstance);
        
        if(isEdit) {
            gui_Text_Field.setText(title);
            gui_Text_Area.setText(content);
            
            setTitle("Edit publication");
            gui_Button_2.setText("Update");
        }
        
        final String[] file = new String[1];
        gui_Button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                Display.getInstance().openGallery(new ActionListener() {
                    public void actionPerformed(ActionEvent ev) {
                        if(ev != null && ev.getSource() != null) {
                            file[0] = (String)ev.getSource();
                    
                        }
                    }
                }, Display.GALLERY_IMAGE);
                
         
            }
        });
        
        gui_Button_2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ev) {
                 FileSystemStorage fs = FileSystemStorage.getInstance();
                  
                try {
                   
                    String fileName = file[0];
                     
                    String title = gui_Text_Field.getText();
                    String content = gui_Text_Area.getText();
                    MultipartRequest multipartRequest = new MultipartRequest() {

                        @Override
                        public void readResponse(InputStream input) throws IOException {
                            
                            System.out.println("respa: " + Util.readToString(input));
                            new NewsFeedForm(resourceObjectInstance).show();
                        }
                    };
                    InfiniteProgress progressIndicator = new InfiniteProgress();
                    Dialog dialog = progressIndicator.showInifiniteBlocking();

                    if(isEdit)
                        multipartRequest.setUrl("http://localhost/PI/web/app_dev.php/rest/news/edit?id="+id);
                    else
                        multipartRequest.setUrl("http://localhost/PI/web/app_dev.php/rest/news/create");
                    
                    multipartRequest.setPost(true);
                   // multipartRequest.setFailSilently(true);
                    //multipartRequest.setTimeout(30000);
                    multipartRequest.addData("image", fileName, "image/jpg");
                    
                    multipartRequest.addArgument("title", title);
                    multipartRequest.addArgument("content", content);
                    //multipartRequest.addArgument("image", fileName);
                    multipartRequest.setFilename("image", fileName);
                    multipartRequest.setFailSilently(false);
                   
                    NetworkManager.getInstance().addToQueueAndWait(multipartRequest);
                    
                    multipartRequest.setDisposeOnCompletion(dialog);
                   
                   
                } catch(Exception e) {
                    e.printStackTrace();
                }
                
                 //showForm();
            }
        });
        
        /*
        
        */
        
    }

//////////////////////////////////////////////////////////////////////////////////////////////////-- DON'T EDIT BELOW THIS LINE!!!
    protected com.codename1.ui.Container gui_Container_1 = new com.codename1.ui.Container(new com.codename1.ui.layouts.BoxLayout(com.codename1.ui.layouts.BoxLayout.Y_AXIS));
    protected com.codename1.ui.TextField gui_Text_Field = new com.codename1.ui.TextField();
    protected com.codename1.ui.TextArea gui_Text_Area = new com.codename1.ui.TextArea();
    protected com.codename1.ui.Button gui_Button = new com.codename1.ui.Button();
    protected com.codename1.ui.Button gui_Button_2 = new com.codename1.ui.Button();


// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initGuiBuilderComponents(com.codename1.ui.util.Resources resourceObjectInstance) {
        setLayout(new com.codename1.ui.layouts.BorderLayout());
        setInlineStylesTheme(resourceObjectInstance);
        setScrollableY(true);
                setInlineStylesTheme(resourceObjectInstance);
        setTitle("Create Publication");
        setName("SignInForm");
        gui_Container_1.setScrollableY(true);
                gui_Container_1.setInlineStylesTheme(resourceObjectInstance);
        gui_Container_1.setName("Container_1");
        addComponent(com.codename1.ui.layouts.BorderLayout.CENTER, gui_Container_1);
        gui_Text_Field.setHint("Title");
                gui_Text_Field.setInlineStylesTheme(resourceObjectInstance);
        gui_Text_Field.setName("Text_Field");
        gui_Text_Area.setHint("Content");
                gui_Text_Area.setInlineStylesTheme(resourceObjectInstance);
        gui_Text_Area.setName("Text_Area");
        gui_Text_Area.setColumns(8);
        gui_Text_Area.setRows(4);
        gui_Button.setText("Browse Image ...");
        gui_Button.setUIID("ButtonGroupOnly");
                gui_Button.setInlineStylesTheme(resourceObjectInstance);
        gui_Button.setName("Button");
        gui_Button_2.setText("Create");
                gui_Button_2.setInlineStylesTheme(resourceObjectInstance);
        gui_Button_2.setName("Button_2");
        gui_Container_1.addComponent(gui_Text_Field);
        gui_Container_1.addComponent(gui_Text_Area);
        gui_Container_1.addComponent(gui_Button);
        gui_Container_1.addComponent(gui_Button_2);
    }// </editor-fold>

//-- DON'T EDIT ABOVE THIS LINE!!!
}
