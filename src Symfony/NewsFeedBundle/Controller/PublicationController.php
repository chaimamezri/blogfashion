<?php

namespace NewsFeedBundle\Controller;

use NewsFeedBundle\Entity\Publication;
use NewsFeedBundle\Entity\PublicationLike;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PublicationController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $publications = $em->getRepository('NewsFeedBundle:Publication')->findAll();

        return $this->render('NewsFeedBundle:Publication:index.html.twig', array(
            'publications' => $publications,
            'userId' => 1,
        ));
    }

    public function createAction(Request $request)
    {
        if($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();

            $publication = new Publication();
            $publication->setUserid(0);

            $file = $request->files->get("image");

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            // Move the file to the directory where brochures are stored
            $dir = $brochuresDir = $this->container->getParameter('kernel.root_dir').'/../web/images/articles';
            $file->move($dir, $fileName);
            //move_uploaded_file($_FILES["image"]["name"], $dir);

            $publication->setTitle($request->get('title'));
            $publication->setContent($request->get('content'));
            $publication->setImage("images/articles/".$fileName);
            $publication->setMediatype(0);
            $publication->setDate(new \DateTime());

            $publication->setViews(0);

            $em->persist($publication);
            $em->flush($publication);

            return $this->redirectToRoute('news_feed_create');
        }

        return $this->render('NewsFeedBundle:Publication:create.html.twig');
    }

    public function deleteAction(Request $request)
    {
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();

        $publication = $em->getRepository('NewsFeedBundle:Publication')->findOneBy(["pubid" => $id]);

        $em->remove($publication);
        $em->flush($publication);

        return $this->redirectToRoute('news_feed_homepage');
    }

    public function editAction(Request $request)
    {
        if($request->isMethod('POST')) {
            $id = $request->get('id');

            $em = $this->getDoctrine()->getManager();

            $publication = $em->getRepository('NewsFeedBundle:Publication')->findOneBy(["pubid" => $id]);

            $publication->setTitle($request->get('title'));
            $publication->setContent($request->get('content'));

            $em->persist($publication);
            $em->flush($publication);

            return $this->redirectToRoute('news_feed_homepage');
        } else {
            $id = $request->get('id');

            $em = $this->getDoctrine()->getManager();

            $publication = $em->getRepository('NewsFeedBundle:Publication')->findOneBy(["pubid" => $id]);

            return $this->render('NewsFeedBundle:Publication:edit.html.twig', array("publication" => $publication));
        }
    }

    public function viewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->get('id');

        $publication = $em->getRepository('NewsFeedBundle:Publication')->findOneBy(["pubid" => $id]);

        $comments = $em->getRepository('NewsFeedBundle:PublicationComment')->findBy(["pubid" => $id]);

        $publication->setViews($publication->getViews() + 1);

        $em->persist($publication);
        $em->flush($publication);

        $userId = 1;

        return $this->render('NewsFeedBundle:Publication:view.html.twig', array(
            'publication' => $publication,
            'comments' => $comments,
            'userId' => $userId
        ));
    }

    public function likeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->get('id');

        $publication = $em->getRepository('NewsFeedBundle:Publication')->findOneBy(["pubid" => $id]);

        $userId = 1;

        if($publication->isLiked($userId)) {
            $pubLike = $em->getRepository('NewsFeedBundle:PublicationLike')->findOneBy(["pubid" => $id,
                "userid" => $userId]);

            $em->remove($pubLike);
            $em->flush();
        } else {
            $pubLike = new PublicationLike();
            $pubLike->setPublication($publication);
            $pubLike->setUserid($userId);

            $em->persist($pubLike);
            $em->flush();
        }
        return $this->redirectToRoute('news_feed_homepage');
    }
}
