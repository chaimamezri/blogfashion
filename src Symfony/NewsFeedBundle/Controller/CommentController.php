<?php


namespace NewsFeedBundle\Controller;


use NewsFeedBundle\Entity\PublicationComment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $pubid = $request->get('id');
        $content = $request->get('content');

        $userId = 1;

        $publication = $em->getRepository('NewsFeedBundle:Publication')->findOneBy(["pubid" => $pubid]);

        $comment = new PublicationComment();
        $comment->setUserid($userId);
        $comment->setPubid($pubid);
        $comment->setPublication($publication);
        $comment->setContent($content);
        $comment->setDate(new \DateTime());

        $em->persist($comment);
        $em->flush($comment);

        return $this->redirectToRoute('news_feed_view', array('id' => $pubid));
    }

    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->get('id');

        $comment = $em->getRepository('NewsFeedBundle:PublicationComment')->findOneBy(["id" => $id]);

        $userId = 1;

        $pubid = $comment->getPubid();

        if($userId == $comment->getUserId()) {
            $em->remove($comment);
            $em->flush($comment);
        }

        return $this->redirectToRoute('news_feed_view', array('id' => $pubid));
    }

    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $id = $request->get('id');
        $content = $request->get('content');

        $comment = $em->getRepository('NewsFeedBundle:PublicationComment')->findOneBy(["id" => $id]);

        $userId = 1;

        $pubid = $comment->getPubid();

        if($userId == $comment->getUserId()) {
            $comment->setContent($content);

            $em->persist($comment);
            $em->flush($comment);
        }

        return $this->redirectToRoute('news_feed_view', array('id' => $pubid));
    }
}