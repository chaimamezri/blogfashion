<?php

namespace NewsFeedBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Publication
 *
 * @ORM\Table(name="publication")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Publication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pubID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pubid;

    /**
     * @var integer
     *
     * @ORM\Column(name="userID", type="integer", nullable=false)
     */
    private $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=2048, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     * @Assert\File(mimeTypes={ "application/image" })
     */
    private $image;

    /**
     * @var file
     *
     * @Vich\UploadableField(mapping="article_images", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @var integer
     *
     * @ORM\Column(name="mediatype", type="integer", nullable=false)
     */
    private $mediatype;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer", nullable=false)
     */
    private $views;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="PublicationLike", mappedBy="publication")
     */
    private $likes;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="PublicationComment", mappedBy="publication")
     */
    private $comments;

    /**
     * @return int
     */
    public function getPubid()
    {
        return $this->pubid;
    }

    /**
     * @param int $pubid
     */
    public function setPubid($pubid)
    {
        $this->pubid = $pubid;
    }

    /**
     * @return int
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param int $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getMediatype()
    {
        return $this->mediatype;
    }

    /**
     * @param int $mediatype
     */
    public function setMediatype($mediatype)
    {
        $this->mediatype = $mediatype;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param int $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }

    /**
     * @return int
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param int $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    public function isLiked($userId) {
        foreach($this->likes as $like) {
            if($like->getUserid() == $userId)
                return true;
        }
        return false;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->mediatype = 0;
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
}

