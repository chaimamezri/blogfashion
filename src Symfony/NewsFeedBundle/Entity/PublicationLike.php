<?php

namespace NewsFeedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicationLike
 *
 * @ORM\Table(name="publication_like")
 * @ORM\Entity
 */
class PublicationLike
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pubID", type="integer", nullable=false)
     */
    private $pubid;

    /**
     * @var integer
     *
     * @ORM\Column(name="userID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userid;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Publication", inversedBy="likes")
     * @ORM\JoinColumn(name="pubID", referencedColumnName="pubID")
     */
    private $publication;

    /**
     * @return int
     */
    public function getPubid()
    {
        return $this->pubid;
    }

    /**
     * @param int $pubid
     */
    public function setPubid($pubid)
    {
        $this->pubid = $pubid;
    }

    /**
     * @return int
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param int $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    /**
     * @return int
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * @param int $publication
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;
    }
}

