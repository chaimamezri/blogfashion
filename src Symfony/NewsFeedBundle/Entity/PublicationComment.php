<?php


namespace NewsFeedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicationComment
 *
 * @ORM\Table(name="publication_comment")
 * @ORM\Entity
 */
class PublicationComment
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="pubID", type="integer", nullable=false)
     */
    private $pubid;

    /**
     * @var integer
     *
     * @ORM\Column(name="userID", type="integer", nullable=false)
     */
    private $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=2048, nullable=false)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Publication", inversedBy="comments")
     * @ORM\JoinColumn(name="pubID", referencedColumnName="pubID")
     */
    private $publication;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getPubid()
    {
        return $this->pubid;
    }

    /**
     * @param int $pubid
     */
    public function setPubid($pubid)
    {
        $this->pubid = $pubid;
    }

    /**
     * @return int
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * @param int $userid
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * @param int $publication
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;
    }
}